package main

import "asyncskribble/server"

func main() {
	e := server.NewServer()
	e.Logger.Fatal(e.Start(":6699"))
}
