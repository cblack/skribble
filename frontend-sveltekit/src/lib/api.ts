import { username } from './store'
import { get } from 'svelte/store'

const endpoint = `/api/v1`

interface Lobby {
    identifier: string
    playerA: string
    playerB: string
}

export async function getLobbies(): Promise<Lobby[]> {
    const data = await fetch(`${endpoint}/lobbies`)
    const body = await data.json()

    if (data.status !== 200) {
        throw "Failed to get lobbies"
    }

    return body as Lobby[]
}

export interface Color {
    r: number;
    g: number;
    b: number;
}

export interface FillEvent {
    x: number
    y: number
    color: Color
}

export interface LineEvent {
    fromX: number
    fromY: number
    toX: number
    toY: number
    lineWidth: number
    color: Color
}

export enum State {
    PlayerADrawing = 0,
    PlayerBGuessing,
    PlayerBDrawing,
    PlayerAGuessing
}

export interface LobbyData {
    playerA: string
    playerB: string
    word: string
    lobbyState: State
    drawing: Event[]
    possibleWords: string[]
}

export type Event = { type: "fill", data: FillEvent } | { type: "line", data: LineEvent }

export async function getLobby(identifier: string): Promise<LobbyData> {
    const data = await fetch(`${endpoint}/lobbies/${identifier}`, {
        headers: {
            "Username": get(username)
        },
    })
    const body = await data.json()

    return body as LobbyData
}

export async function updateLobby(identifier: string, drawing: Event[] | null): Promise<void> {
    const data = await fetch(`${endpoint}/lobbies/${identifier}`, {
        method: "POST",
        headers: {
            "Username": get(username),
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            "drawing": drawing,
        })
    })

    if (data.status !== 200) {
        throw "Failed to update lobby"
    }
}

export async function createLobby(identifier: string, mit: string): Promise<void> {
    const data = await fetch(`${endpoint}/newLobby`, {
        method: "POST",
        headers: {
            "Username": get(username),
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            "identifier": identifier,
            "with": mit,
        })
    })
    if (data.status !== 200) {
        throw "Failed to create lobby"
    }
}
