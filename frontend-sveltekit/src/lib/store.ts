import { browser } from '$app/env'
import { writable } from 'svelte/store'

const storedUsername: string | null = browser ? localStorage.getItem("username") : null
export const username = writable(storedUsername)
username.subscribe(value => {
    if (!browser) return

    localStorage.setItem("username", value)
})
