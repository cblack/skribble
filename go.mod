module asyncskribble

go 1.16

require (
	github.com/alecthomas/repr v0.0.0-20210801044451-80ca428c5142
	github.com/labstack/echo/v4 v4.5.0
)
