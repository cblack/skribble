package state

import "math/rand"

type LobbyState int

const (
	PlayerADrawing LobbyState = iota
	PlayerBGuessing
	PlayerBDrawing
	PlayerAGuessing
)

func (l LobbyState) Next() LobbyState {
	switch l {
	case PlayerADrawing:
		return PlayerBGuessing
	case PlayerBGuessing:
		return PlayerBDrawing
	case PlayerBDrawing:
		return PlayerAGuessing
	case PlayerAGuessing:
		return PlayerADrawing
	}
	panic("bad value")
}

type Event struct {
	Type string      `json:"type"`
	Data interface{} `json:"data"`
}

type Lobby struct {
	PlayerA       Username   `json:"playerA"`
	PlayerB       Username   `json:"playerB"`
	Word          Word       `json:"word"`
	State         LobbyState `json:"lobbyState"`
	Drawing       []Event    `json:"drawing"`
	PossibleWords []Word     `json:"possibleWords"`
}

type Username string

type Word string

var DefaultWords = []Word{
	"konqui",
	"kde",
	"plasma",
	"kate",
	"neochat",
	"gnome",
	"linux",
}

func RandomWord(w []Word) Word {
	return w[rand.Intn(len(w))]
}
