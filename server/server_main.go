package server

import (
	"asyncskribble/state"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type Server struct {
	*echo.Echo

	Lobbies map[string]state.Lobby
}

func load() map[string]state.Lobby {
	data, err := ioutil.ReadFile("db.json")
	if err != nil && os.IsNotExist(err) {
		return map[string]state.Lobby{}
	} else if err != nil {
		panic(err)
	}

	var l map[string]state.Lobby
	err = json.Unmarshal(data, &l)
	if err != nil {
		panic(err)
	}

	return l
}

func save(l map[string]state.Lobby) {
	data, err := json.Marshal(l)
	if err != nil {
		panic(err)
	}

	ioutil.WriteFile("db.json", data, os.ModePerm)
}

func NewServer() *Server {
	e := &Server{
		Echo:    echo.New(),
		Lobbies: load(),
	}
	e.Use(middleware.CORS())

	defer save(e.Lobbies)

	go func() {
		for {
			time.Sleep(2 * time.Minute)
			save(e.Lobbies)
		}
	}()

	e.Static("/", "./static")
	e.HTTPErrorHandler = func(err error, c echo.Context) {
		httpError, ok := err.(*echo.HTTPError)
		if ok {
			errorCode := httpError.Code
			switch errorCode {
			case http.StatusNotFound:
				c.File("./static/index.html")
				return
			}
		}
		e.DefaultHTTPErrorHandler(err, c)
	}
	e.POST("/api/v1/newLobby", e.CtxHandler(e.NewLobby, RouteOpts{
		NeedsUsername: true,
	}))
	e.GET("/api/v1/lobbies", e.CtxHandler(e.DoLobbies, RouteOpts{
		NeedsUsername: false,
	}))
	e.POST("/api/v1/lobbies/:lobby_id", e.CtxHandler(e.UpdateLobby, RouteOpts{
		NeedsUsername: true,
		NeedsLobby:    true,
	}))
	e.GET("/api/v1/lobbies/:lobby_id", e.CtxHandler(e.Lobby, RouteOpts{
		NeedsUsername: true,
		NeedsLobby:    true,
	}))

	return e
}
