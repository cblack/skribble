package server

import (
	"asyncskribble/state"
	"net/http"
)

type NewLobbyRequest struct {
	Identifier string         `json:"identifier"`
	With       state.Username `json:"with"`
	WordList   []state.Word   `json:"words"`
}

func (s *Server) NewLobby(c Context) error {
	var req NewLobbyRequest
	if err := c.Bind(&req); err != nil {
		return err
	}

	_, ok := s.Lobbies[req.Identifier]
	if ok {
		return c.JSON(http.StatusBadRequest, errorAlreadyExists)
	}

	s.Lobbies[req.Identifier] = state.Lobby{}

	words := state.DefaultWords
	if req.WordList != nil {
		words = req.WordList
	}

	s.Lobbies[req.Identifier] = state.Lobby{
		PlayerA:       c.Username,
		PlayerB:       req.With,
		Word:          state.RandomWord(words),
		PossibleWords: words,
	}

	return c.JSON(http.StatusOK, nil)
}
