package server

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/alecthomas/repr"
	"github.com/labstack/echo/v4"
)

type testRequest struct {
	Endpoint string
	Method   string
	Headers  http.Header

	InputJSON interface{}

	ExpectedCode     int
	ExpectedResponse interface{}
}

var (
	newLobbyTest = []testRequest{
		{
			Endpoint: "/api/v1/newLobby",
			Method:   http.MethodPost,
			Headers:  http.Header{},

			InputJSON: NewLobbyRequest{
				Identifier: "foo",
				With:       "bar",
			},

			ExpectedCode:     http.StatusUnauthorized,
			ExpectedResponse: errorNeedsUsername,
		},
		{
			Endpoint: "/api/v1/newLobby",
			Method:   http.MethodPost,
			Headers: http.Header{
				"Username": {"pontaoski"},
			},

			InputJSON: NewLobbyRequest{
				Identifier: "foo",
				With:       "bar",
			},

			ExpectedCode:     http.StatusOK,
			ExpectedResponse: nil,
		},
		{
			Endpoint: "/api/v1/newLobby",
			Method:   http.MethodPost,
			Headers: http.Header{
				"Username": {"pontaoski"},
			},

			InputJSON: NewLobbyRequest{
				Identifier: "foo",
				With:       "bar",
			},

			ExpectedCode:     http.StatusBadRequest,
			ExpectedResponse: errorAlreadyExists,
		},
	}
	lobbiesTest = []testRequest{
		{
			Endpoint: "/api/v1/newLobby",
			Method:   http.MethodPost,
			Headers: http.Header{
				"Username": {"pontaoski"},
			},

			InputJSON: NewLobbyRequest{
				Identifier: "foo",
				With:       "bar",
			},

			ExpectedCode:     http.StatusOK,
			ExpectedResponse: nil,
		},
		{
			Endpoint: "/api/v1/newLobby",
			Method:   http.MethodPost,
			Headers: http.Header{
				"Username": {"pontaoski"},
			},

			InputJSON: NewLobbyRequest{
				Identifier: "fun",
				With:       "bar",
			},

			ExpectedCode:     http.StatusOK,
			ExpectedResponse: nil,
		},
		{
			Endpoint: "/api/v1/lobbies",
			Method:   http.MethodGet,

			ExpectedCode: http.StatusOK,
			ExpectedResponse: []LobbiesResponse{
				{
					Identifier: "foo",
					PlayerA:    "pontaoski",
					PlayerB:    "bar",
				},
				{
					Identifier: "fun",
					PlayerA:    "pontaoski",
					PlayerB:    "bar",
				},
			},
		},
	}
	lobbyTest = []testRequest{
		{
			Endpoint: "/api/v1/newLobby",
			Method:   http.MethodPost,
			Headers: http.Header{
				"Username": {"pontaoski"},
			},

			InputJSON: NewLobbyRequest{
				Identifier: "foo",
				With:       "bar",
			},

			ExpectedCode:     http.StatusOK,
			ExpectedResponse: nil,
		},
		{
			Endpoint: "/api/v1/lobbies/foo",
			Method:   http.MethodGet,
			Headers: http.Header{
				"Username": {"pontaoski"},
			},

			ExpectedCode:     http.StatusOK,
			ExpectedResponse: nil,
		},
		{
			Endpoint: "/api/v1/lobbies/foo",
			Method:   http.MethodGet,
			Headers: http.Header{
				"Username": {"bar"},
			},

			ExpectedCode:     http.StatusOK,
			ExpectedResponse: nil,
		},
		{
			Endpoint: "/api/v1/lobbies/foo",
			Method:   http.MethodGet,
			Headers: http.Header{
				"Username": {"eee"},
			},

			ExpectedCode:     http.StatusForbidden,
			ExpectedResponse: errorNotInGame,
		},
	}
)

func runTest(items []testRequest, t *testing.T) {
	e := NewServer()

	for _, it := range items {

		data, err := json.Marshal(it.InputJSON)
		if err != nil {
			panic(err)
		}

		req := httptest.NewRequest(it.Method, it.Endpoint, bytes.NewReader(data))
		req.Header = it.Headers
		if req.Header == nil {
			req.Header = http.Header{}
		}
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		rec := httptest.NewRecorder()

		c := e.NewContext(req, rec)
		e.Router().Find(it.Method, it.Endpoint, c)
		c.Handler()(c)

		if it.ExpectedCode != rec.Code {
			t.Errorf("Expected status code: %d\nGot status code: %d", it.ExpectedCode, rec.Code)
		}

		dat, err := ioutil.ReadAll(rec.Body)
		if err != nil {
			panic(err)
		}

		if it.ExpectedResponse == nil {
			continue
		}

		kind := reflect.TypeOf(it.ExpectedResponse)
		resp := reflect.New(kind)

		err = json.Unmarshal(dat, resp.Interface())
		if err != nil {
			panic(err)
		}

		val := resp.Elem().Interface()

		if !reflect.DeepEqual(val, it.ExpectedResponse) {
			t.Errorf("Expected response: %s\nGot response: %s", repr.String(it.ExpectedResponse), repr.String(val))
		}
	}
}

func TestNewLobby(t *testing.T) {
	runTest(newLobbyTest, t)
}

func TestLobbies(t *testing.T) {
	runTest(lobbiesTest, t)
}

func TestLobby(t *testing.T) {
	runTest(lobbyTest, t)
}
