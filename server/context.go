package server

import (
	"asyncskribble/state"
	"net/http"

	"github.com/labstack/echo/v4"
)

type Context struct {
	echo.Context

	Username state.Username
	LobbyID  string
	Lobby    state.Lobby
}

type RouteOpts struct {
	NeedsUsername bool
	NeedsLobby    bool
}

func (s *Server) CtxHandler(fn func(c Context) error, opts RouteOpts) func(echo.Context) error {
	return func(c echo.Context) error {
		ctx := Context{Context: c}

		if opts.NeedsUsername {
			username := c.Request().Header.Get("Username")
			if username == "" {
				return c.JSON(http.StatusUnauthorized, errorNeedsUsername)
			}
			ctx.Username = state.Username(username)
		}

		if opts.NeedsLobby {
			l, ok := s.Lobbies[c.Param("lobby_id")]
			if !ok {
				return c.JSON(http.StatusNotFound, errorLobbyNotFound)
			}

			if ctx.Username != l.PlayerA && ctx.Username != l.PlayerB {
				return c.JSON(http.StatusForbidden, errorNotInGame)
			}

			ctx.LobbyID = c.Param("lobby_id")
			ctx.Lobby = l
		}

		return fn(ctx)
	}
}
