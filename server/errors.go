package server

const (
	errorNeedsUsername = "s.needs-username"
	errorAlreadyExists = "s.already-exists"
	errorNotInGame     = "s.not-in-game"
	errorLobbyNotFound = "s.lobby-not-found"
)
