package server

import (
	"net/http"
)

func (s *Server) Lobby(c Context) error {
	return c.JSON(http.StatusOK, c.Lobby)
}
