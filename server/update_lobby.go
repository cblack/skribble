package server

import (
	"asyncskribble/state"
	"net/http"
)

type UpdateLobbyRequest struct {
	Drawing []state.Event `json:"drawing"`
}

func (s *Server) UpdateLobby(c Context) error {
	var req UpdateLobbyRequest
	if err := c.Bind(&req); err != nil {
		return err
	}

	d := c.Lobby

	isA := c.Username == d.PlayerA
	isB := c.Username == d.PlayerB

	if isA && d.State == state.PlayerADrawing {
		d.Drawing = req.Drawing
	} else if isB && d.State == state.PlayerBDrawing {
		d.Drawing = req.Drawing
	} else if (isB && d.State == state.PlayerBGuessing) || (isA && d.State == state.PlayerAGuessing) {
		d.Word = state.RandomWord(d.PossibleWords)
		d.Drawing = nil
	}

	d.State = d.State.Next()

	s.Lobbies[c.LobbyID] = d

	return c.JSON(http.StatusOK, nil)
}
