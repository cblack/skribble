package server

import (
	"asyncskribble/state"
	"net/http"
)

type LobbiesResponse struct {
	Identifier string         `json:"identifier"`
	PlayerA    state.Username `json:"playerA"`
	PlayerB    state.Username `json:"playerB"`
}

func (s *Server) DoLobbies(c Context) error {
	lobbies := make([]LobbiesResponse, len(s.Lobbies))
	i := -1

	for key, lobby := range s.Lobbies {
		i++

		lobbies[i] = LobbiesResponse{
			Identifier: key,
			PlayerA:    lobby.PlayerA,
			PlayerB:    lobby.PlayerB,
		}
	}

	return c.JSON(http.StatusOK, lobbies)
}
